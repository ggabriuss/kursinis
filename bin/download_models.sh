#! /bin/bash

test -d input_models || mkdir input_models
test -d downloaded_tars || mkdir downloaded_tars

cat casp12.models | grep -v '^#' | awk -F"\n" '{print $1}' | while read ID; do((set -x; whoami;hostname -f; date; curl -sSL http://predictioncenter.org/download_area/CASP12/predictions/$ID.tar.gz > downloaded_tars/$ID.tar.gz; tar zxvpf downloaded_tars/$ID.tar.gz -C input_models/ ) 2>&1 | tee input_models/$ID.log) done


