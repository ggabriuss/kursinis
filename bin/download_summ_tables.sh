#! /bin/bash

test -d summary_tables || mkdir summary_tables

cat casp12.models | grep -v '^#' | awk -F"\n" '{print $1}' | while read ID; do((set -x; whoami;hostname -f; date; curl -sSL http://predictioncenter.org/download_area/CASP12/SUMMARY_TABLES/$ID.txt > summary_tables/$ID.txt) 2>&1 | tee summary_tables/$ID.log) done

