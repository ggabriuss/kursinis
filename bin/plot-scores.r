#! /usr/bin/env Rscript 

# Šiam failui turi būti paduodami 2 argumentai.
#Primasis - failas kuriame guli iverciai
# Antrasis - pdf output failas

args = commandArgs(trailingOnly=TRUE)
if (length(args)!=2) {
  stop("Blogas argumentų skaičius", call.=FALSE)
}

name = basename(args[2])
mydata = read.table(args[1], header=TRUE, sep="", dec=".", strip.white=TRUE, comment.char = "")
mydata$X2Djury = round(scale(mydata$X2Djury), digits = 2)
pdf(args[2])
hist(mydata$X2Djury, main = paste(name, " iverciu pasiskirstymas"),
     xlab = "Iverciai", ylab = "Daznis")
