#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long qw(GetOptions);
use Data::Dumper;

sub genImage{
	my ($size, $columns, $rows, $chains, $file) = (shift, shift, shift, shift, shift);
	my $header = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 $size $size\">\n";
	if(defined $file){
		open (FH, "> $file") || die "problem opening $file\n";
		print FH $header;
	}else{print $header;}
	for(my $i=0;$i<scalar(@{$columns});$i=$i+2){
		my @shiftx = (grep{$$columns[$i+1] eq $$chains{$_}} keys %{$chains});
		my @shifty = (grep{$$rows[$i+1] eq $$chains{$_}} keys %{$chains});
		my $x = $$columns[$i]+ $shiftx[0];
		my $y = $$rows[$i]+ $shifty[0];
		my $print = "<rect x=\"$x\" y=\"$y\" width=\"1\" height=\"1\"/>\n";
		$print = $print . "<rect x=\"$y\" y=\"$x\" width=\"1\" height=\"1\"/>\n";
		if(defined $file){
			print FH $print;
		}else{print $print;}
	}	
	if(defined $file){
		print FH "</svg>"; close(FH);
	}else{print "</svg>";}
	return;

};

my $fname; # name of the output file
my @imageCols;
my @imageRows;
my $chainId=""; #stores chainId, empty = initial value that does not match any chainId
my $aminosInchain=0; # counts the number of residues in a particular chain
my %chainArr; # hash that stores chainID's and the number of residues belonging to that chain 
my $imgSize=3; # imageSize= total number of residues+3, starts at 3 for slightly bigger picture
my $uniqueAmino=0.01; # stores current residue number, 0.01 = initial value that does not match any residue number
my $shift=0; # how much to shift for printing different chain sequences
GetOptions('output|o=s' => \$fname) or die "Usage: $0 [--output file.svg]\n";

while(<>){
	if($_ =~ /ChainID1/){next;}
	chomp;
	my @split = split( /\s+/, $_ );
	if($uniqueAmino != $split[1]){
		if($chainId ne $split[0]){
			$chainId=$split[0];
			$shift = $shift + $aminosInchain;
			$chainArr{$shift} = $chainId;
			$aminosInchain=0;
		}
		$uniqueAmino=$split[1];
		$imgSize++;
		$aminosInchain++;
	}
	push @imageCols, $split[1]; push @imageCols, $split[0];
	push @imageRows, $split[5]; push @imageRows, $split[4];
}
genImage($imgSize, \@imageCols, \@imageRows, \%chainArr, $fname);

