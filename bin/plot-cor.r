#! /usr/bin/env Rscript 

# Šiam failui turi būti paduodami 3 argumentai.
#Primasis - failas kuriame guli koreliacijos koeficientai 
# Antrasis - įverčio su kuriuo lyginama pavadinimas.
# Treciasis - pdf output failas

args = commandArgs(trailingOnly=TRUE)
if (length(args)!=3) {
  stop("Blogas argumentų skaičius", call.=FALSE)
}

cors = scan(args[1], skip = 1)
pdf(args[3])
hist(cors, breaks = 10, main = paste(args[2],
  " ir 2Djury koreliacijos pasiskirstymas"),
     xlab = paste("Koreliacijos koeficientai", "\nVidurkis = ",
                  mean(cors)), ylab = "Daznis")

#cors = read.table(args[1], header=TRUE, sep="", dec=".", strip.white=TRUE)
#library(ggplot2)
#plot = ggplot(cors, aes(cors[[1]])) + geom_density(fill="red")
#ggsave(plot,file=args[3])













