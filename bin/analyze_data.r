#! /usr/bin/env Rscript 

# Šiam failui turi būti paduodami 3 argumentai.
#Primasis - summary_table
# Antrasis - savieji įverčiai.
# Treciasis - merged_scores output failas

args = commandArgs(trailingOnly=TRUE)
if (length(args)!=3) {
  stop("Blogas argumentų skaičius", call.=FALSE)
}

mydata = read.table(args[1], header=TRUE, sep="", dec=".", strip.white=TRUE, comment.char = "")
mydata = subset(mydata, select = c(Model, GDT_TS, LDDT, CAD_AA))
mydata = subset(mydata, grepl("_1$",mydata$Model))
myscore = read.table(args[2], header=TRUE, sep="", dec=".", strip.white=TRUE)

#myscore$X2Djury = round(scale(myscore$X2Djury), digits = 2)
joined = merge(mydata, myscore, by.x = "Model", by.y = "Model", all = TRUE, sort = TRUE)
joined = na.omit(joined)
outName1 = paste(dirname(args[3]), "/Rplots.pdf", sep = "")
outName2 = paste(dirname(args[3]), "/statistics.txt", sep = "")
pdf(outName1)

tex = "Koreliacijos koeficientai:\n"
for (i in 2:(length(names(joined))-1)){
  plot(joined$X2Djury, joined[[i]], main = paste("2Djury vs",names(joined)[i], sep =" "), xlab = "2Djury", ylab = names(joined)[i])
  tex0 = paste("2Djury vs",names(joined)[i], sep =" ")
  tex1 = paste("pearson", cor(joined$X2Djury, joined[[i]], method = "pearson"), sep = " ")
  tex2 = paste("spearman", cor(joined$X2Djury, joined[[i]], method = "spearman"), "\n", sep = " ")
  tex = paste(tex, tex0, tex1, tex2, sep = "\n")
}
write(tex, file = outName2, append = FALSE)
capture.output(print(joined, sep="\t", row.names=FALSE, quote=FALSE), file=args[3])

# tex1 = paste("pearson", cor(joined$my_score, joined[[i]], method = "pearson", use = "complete.obs"), sep = " ")





