#! /usr/bin/env Rscript 

# Šiam failui turi būti paduodami 3 argumentai.
# Primasis - failas kuriame guli visu failu iverciai 
# Antrasis - ivercio pavadinimas
# Treciasis - pdf output failas

args = commandArgs(trailingOnly=TRUE)
if (length(args)!=3) {
  stop("Blogas argumentų skaičius", call.=FALSE)
}


mydata = read.table(args[1], header=TRUE, sep="", dec=".", strip.white=TRUE, na.strings = args[2])
mydata = na.omit(mydata)
mydata[[1]] = round(scale(mydata[[1]]), digits = 2)
pdf(args[3])
hist(mydata[[1]], main = paste(args[2], " iverciu pasiskirstymas"),
     xlab = paste("Iverciai", 
                  "\nMin = ", min(mydata[[1]]),
                  " Max = ", max(mydata[[1]])), ylab = "Daznis")