#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long qw(GetOptions);
use Data::Dumper;


# Function that calculates rgb colors, from precentage of models having a specific contact - $int; bigger likelyhood
# of contact being in all models - more red, the smaller - more blue; The middle or 50% is white. 
sub convertIntensityToColor {
	my $int = shift; 
	my $red = 0;
	my $green = 0;
	my $blue = 0;
	if($int <= 0.5 ){ # getting more blue
		$blue = 1;
		$green =  $int / 0.5;
		$red = $green;
	}else{ #getting more red
		$red = 1;
		$green =  (1 - $int) / 0.5;
		$blue = $green;
	}
	$red = $red * 100;
	$green = $green * 100;
	$blue = $blue * 100;
	return "rgb($red%,$green%,$blue%)";
};

sub genImage{
	my ($size, $columns, $rows, $chains, $intensity, $file) = (shift, shift, shift, shift, shift, shift);
	my $header = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 $size $size\">\n";
	$header = $header . "<rect x=\"0\" y=\"0\" width=\"$size\" height=\"$size\" fill=\"black\"  />\n";
	if(defined $file){
		open (FH, "> $file") || die "problem opening $file\n";
		print FH $header;
	}else{print $header;}
	for(my $i=0;$i<scalar(@{$columns});$i=$i+2){
		my @shiftx = (grep{$$columns[$i+1] eq $$chains{$_}} keys %{$chains});
		my @shifty = (grep{$$rows[$i+1] eq $$chains{$_}} keys %{$chains});
		my $x = $$columns[$i]+ $shiftx[0];
		my $y = $$rows[$i]+ $shifty[0];
		my $color = convertIntensityToColor($$intensity[$i/2]); 
		my $print = "<rect x=\"$x\" y=\"$y\" width=\"1\" height=\"1\" fill=\"$color\"/>\n";
		$print = $print . "<rect x=\"$y\" y=\"$x\" width=\"1\" height=\"1\" fill=\"$color\"/>\n";
		$x= $i/2;
		$color = $x > $size-3 ? "black" : "red"; 
		$print = $print . "<rect x=\"$x\" y=\"$x\" width=\"1\" height=\"1\" fill=\"$color\"/>\n";
		if(defined $file){
			print FH $print;
		}else{print $print;}
	}	
	if(defined $file){
		print FH "</svg>"; close(FH);
	}else{print "</svg>";}
	return;

};



my $fname; # name of the output file
my @imageCols;
my @imageRows;
my @imageColors; # variables for every residue pair, that will determine the color of contact in genImage();
my $chainId=""; #stores chainId, empty = initial value that does not match any chainId
my $aminosInchain=0; # counts the number of residues in a particular chain
my %chainArr; # hash that stores chainID's and the number of residues belonging to that chain 
my $imgSize=3; # imageSize= total number of residues+3, starts at 3 for slightly bigger picture
my $uniqueAmino=0.01; # stores current residue number, 0.01 = initial value that does not match any residue number
my $shift=0; # how much to shift for printing different chain sequences
GetOptions('output|o=s' => \$fname) or die "Usage: $0 [--output file.svg]\n";

while(<>){
	if($_ =~ /ChainID1/){next;}
	chomp;
	$_ =~ /(\S+\s+){8}/;
	my @models = split( /\s+/, $' );
	my @residueInfo = split( /\s+/, $& );
	my $hits = scalar((grep {$_ =~ /\d/} @models));

	$hits=$hits/scalar(@models);	
	if($uniqueAmino != $residueInfo[1]){
		if($chainId ne $residueInfo[0]){
			$chainId=$residueInfo[0];
			$shift = $shift + $aminosInchain;
			$chainArr{$shift} = $chainId;
			$aminosInchain=0;
		}
		$uniqueAmino=$residueInfo[1];
		$imgSize++;
		$aminosInchain++;
	}
	push @imageCols, $residueInfo[1]; push @imageCols, $residueInfo[0];
	push @imageRows, $residueInfo[5]; push @imageRows, $residueInfo[4];
	push @imageColors, $hits;
}
genImage($imgSize, \@imageCols, \@imageRows, \%chainArr, \@imageColors,  $fname);

