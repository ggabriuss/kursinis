#! /bin/bash

set -ue

args=("$@")
model=${args[0]} 
model=${model%/*}
model=${model##*/}

OUTPUT_DIR="../maps/${model}"
FILES=""
while [ $# -gt 0 ]
do
	case "$1" in
		-o)	OUTPUT_DIR=$2
			shift
			;;
		--help|--hel|--he|--h)
			echo USAGE: "$0 1zyx.pdb [2zyx.pdb ...] -o /1zyx_maps"
			exit 0
			;;
		-*) 	echo "$0 unrecognised option '$1'" >&2
			exit 1
			;;
		*) 	FILES="${FILES} '$1'"
			;;
	esac 
	shift
done
 #gražina parametrus
eval set -- "${FILES}"

test -d ${OUTPUT_DIR} || mkdir ${OUTPUT_DIR}
   
#  $# = ${#args[@]}
for ((i=0; i<$# ;i++)) 
do
outname=$(basename ${args[i]}) 
./genmap ${args[i]} | column -t > ${OUTPUT_DIR}/${outname}_map
done

BIN_DIR="../bin"
MERGED_DIR="../merged_maps"
GRAPH_DIR="../map_plots"
SUMMARY_DIR="../summary_tables"
SCORES_DIR="../scores"
COMPARISON_DIR="../comparison"

./mergemaps ${OUTPUT_DIR}/* | column -t > ${MERGED_DIR}/merged_${model}.txt
./colormap.pl ${MERGED_DIR}/merged_${model}.txt -o ${GRAPH_DIR}/${model}.svg
./evalmodels ${MERGED_DIR}/merged_${model}.txt | column -t > ${SCORES_DIR}/${model}.txt
cd ${COMPARISON_DIR}
test -d ${model} || mkdir ${model}
cd ${model}
Rscript ../${BIN_DIR}/analyze_data.r ../${SUMMARY_DIR}/${model}.txt ../${SCORES_DIR}/${model}.txt

