genmap (generate map) - generuoja baltymų kontaktų žemėlapius, pagal amino rugščių atstumą (jis apibrėžtas kaip atstumas tarp amino rugščių atomų, kurių atstumas trumpiausias) . Yra trys opcijos: -o skirta išvesties failui nurodyti, -c nurodo maksimalų atstumą tarp alpha anglies atomų, -r nurodo maksimalų atstumą tarp amino rugščių liekanų. Visos trys opcijos nėra privalomos, turi default reikšmes, jeigu nenurodytas išvesties failas, rašoma į standartinę išvestį.

map2svg.pl - programa skirta iš genmap sugeneruotų kontaktų žemėlapių sukurti svg paveikslėlius. Turi vieną neprivalomą opciją -o skirtą nurodyti išvesties failą.

mergemaps - genmap sugeneruotus failus sujungia į vieną bendrą failą. Ima failų sąrašą ir į standartinę išvestį atspauzdina bendrą lentelę.

colormap.pl - panaši į map2svg.pl programą, bet skrita sugeneruoti ir nuspalvinti mergemaps pagamintą bendrą kontaktų žemėlapį. Kontaktai
splavinami, pagal tai kiek modelių pasireiškė tas kontaktas. Kuo šviesesnė spalva, tuo dažniau kontaktas kartojasi modeliuose, kuo tamsesnė spalva, tuo mažiau pasireiškia.

genmany.sh - skriptas, kuriam paduodamas pdb failų sąrašas, iš jų pagaminama kontaktų žemėlapiai su genmap programa, jie sujungiami į
bendrą lentelę su mergemaps programa, ir sugeneruojamas svg failas - nuspalvintas bendras įvestų pdb failų kontaktų žemėlapis. Programai galima nurodyti kontaktų žemėlapių išvedimo direktoriją su -o opcija.

genmap ir mergemaps išvestį patariama pateikti column -t komandai.

Programų naudojimo pavyzdžiai:

./genmap data/1vad.pdb -o out.txt

./genmap data/1vad.pdb | column -t > out.txt

cat out.txt | ./map2svg.pl -o out.svg

./genmap -r 7 -c 15 T0860/ACOMPMOD_TS1 | column -t | ./map2svg.pl -o out2.svg

./mergemaps ../T0860/* | column -t > merged_T0860.txt

./colormap.pl merged_T0860.txt -o T0860.svg

./mergemaps ../T0859/* | column -t | ./colormap.pl > T0859.svg

./run.sh ../T0859/* -o ../results_T0859

Makefile - atlieka visą analizę. Skirtas nukelti pdb failus, summary_tables.txt iš casp projekto, 
sugeneruoti kiekvienam failui kontaktų žemėlapį, naudojant 
joint algoritmą sudaryti bendras jų lenteles, nubraižytį jų grafikus,
suskaičiuoti nukeltų modelių įverčius remiantis 1D-jury ir palyginti
su GDT_TS  LDDT CAD_AA įverčiais, randant koreliacijos koeficentus.
Naudojimas:

make downloads

make all

