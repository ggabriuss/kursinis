#--*- Makefile -*--

# Makefile skirtas nukelti pdb failus, summary_tables.txt iš casp projekto, 
# sugeneruoti kiekvienam failui kontaktų žemėlapį, naudojant 
# joint algoritmą sudaryti bendras jų lenteles, nubraižytį grafikus,
# suskaičiuoti nukeltų modelių įverčius remiantis 1D-jury ir palyginti
# su GDT_TS, LDDT, CAD_AA įverčiais, randant koreliacijos koeficentus.

# Naudojimas:
#
# make
# make all
# make downloads  
# make clean # Ištrinami suskaičiuoti failai
# make distclean # Ištrinami visi

# Projekto direktorijos:
BIN_DIR = bin
MERGED_DIR = merged_maps
GRAPH_DIR = map_plots
SUMMARY_DIR = summary_tables
SCORES_DIR = scores
INPUT_DIR = input_models
MAP_DIR = maps
COMPARISON_DIR = comparison
COR_ANALYSIS_DIR = correlation_analysis
SCORE_ANALYSIS_DIR = score_analysis

# Kintamieji, generuojami failai, naudojamos direktorijos
CASP = casp12
CASP_ID_FILE = $(wildcard ${CASP}.models)
TARGET_LIST = $(shell cat ${CASP_ID_FILE})
ALL_INPUT_TARGET_DIRS = $(sort $(wildcard ${INPUT_DIR}/*))

ALL_COR_STATS = ${TARGET_LIST:%=${COMPARISON_DIR}/%/statistics.txt}

MODEL_FILES := $(foreach dir,$(ALL_INPUT_TARGET_DIRS),$(wildcard $(dir)/*_1))
CONTACT_MAPS = ${MODEL_FILES:${INPUT_DIR}/%=${MAP_DIR}/%_map}

MERGED_MAPS = ${TARGET_LIST:%=${MERGED_DIR}/merged_%.txt}
MERGED_MAP_PLOTS = ${TARGET_LIST:%=${GRAPH_DIR}/%.svg}
SUMMARY_FILES = ${TARGET_LIST:%=${SUMMARY_DIR}/%.txt}
TARGET_SCORES = ${TARGET_LIST:%=${SCORES_DIR}/%.txt}

MERGED_SCORES = ${TARGET_LIST:%=${COMPARISON_DIR}/%/merged_scores.txt}
SCORE_HIST =  ${TARGET_LIST:%=${SCORE_ANALYSIS_DIR}/%.pdf}


JOINT_2Djury = ${SCORE_ANALYSIS_DIR}/joint_2Djury_scores.txt
BENDRAS_2Djury = ${SCORE_ANALYSIS_DIR}/overall_2Djury.pdf
MY_SCORE = ${JOINT_2Djury} ${BENDRAS_2Djury}

JOINT_CAD_AA = ${SCORE_ANALYSIS_DIR}/joint_CAD_AA_scores.txt
BENDRAS_CAD_AA = ${SCORE_ANALYSIS_DIR}/overall_CAD_AA.pdf
CAD_AA = ${JOINT_CAD_AA} ${BENDRAS_CAD_AA}

JOINT_LDDT = ${SCORE_ANALYSIS_DIR}/joint_LDDT_scores.txt
BENDRAS_LDDT = ${SCORE_ANALYSIS_DIR}/overall_LDDT.pdf
LDDT = ${JOINT_LDDT} ${BENDRAS_LDDT}

JOINT_GDT_TS = ${SCORE_ANALYSIS_DIR}/joint_GDT_TS_scores.txt
BENDRAS_GDT_TS = ${SCORE_ANALYSIS_DIR}/overall_GDT_TS.pdf
GDT_TS = ${JOINT_GDT_TS} ${BENDRAS_GDT_TS}

CORRELATION = "pearson"
#CORRELATION = "spearman"

#SCORES = GDT_TS LDDT CAD_AA
#SCORE_CORS = ${SCORES:%=${COR_ANALYSIS_DIR}/%cors.txt}
GDT_TS_CORS = ${COR_ANALYSIS_DIR}/GDT_TScors.txt
LDDT_CORS = ${COR_ANALYSIS_DIR}/LDDTcors.txt
CAD_AA_CORS = ${COR_ANALYSIS_DIR}/CAD_AAcors.txt

ABC = ${COR_ANALYSIS_DIR}/a.pdf ${COR_ANALYSIS_DIR}/b.pdf ${COR_ANALYSIS_DIR}/c.pdf
COR_PLOTS = ${COR_ANALYSIS_DIR}/cor_plots.pdf



.PHONY: all clean distclean display downloads mainAnalysis corAnalysis scoreAnalysis

.PRECIOUS: ${MERGED_MAPS}

all: mainAnalysis scoreAnalysis corAnalysis 

mainAnalysis: ${CONTACT_MAPS} ${MERGED_MAP_PLOTS} ${TARGET_SCORES}

corAnalysis: ${GDT_TS_CORS} ${LDDT_CORS} ${CAD_AA_CORS} ${COR_PLOTS}

scoreAnalysis: ${MERGED_SCORES} ${SCORE_HIST} ${MY_SCORE} ${CAD_AA} ${LDDT} ${GDT_TS}


display: 
	@echo ${MERGED_SCORES} 

downloads:
	${BIN_DIR}/download_summ_tables.sh
	${BIN_DIR}/download_models.sh

#------------------------------------------------------------------------------
#Iš pradinių modelių failų, generuojami kontaktų žemėlapių failai:

${MAP_DIR}/%_map: ${INPUT_DIR}/%
	@mkdir -p $(dir $@)
	${BIN_DIR}/genmap $< | column -t > $@

#------------------------------------------------------------------------------
#Iš bendrų kontaktų žemėlapių lentelių, kuriami spalvoti brėžiniai ir įverčiai:

${GRAPH_DIR}/%.svg: ${MERGED_DIR}/merged_%.txt
	${BIN_DIR}/colormap.pl $< -o $@


${SCORES_DIR}/%.txt: ${MERGED_DIR}/merged_%.txt
	${BIN_DIR}/evalmodels $< | column -t > $@

#------------------------------------------------------------------------------
# Bendrų kontaktų žemėlapių lentelių kurimas:

${MERGED_DIR}/merged_%.txt: ${MAP_DIR}/%
	${BIN_DIR}/mergemaps $</* | column -t > $@

#------------------------------------------------------------------------------
# Koreliacijos koeficientų surinkimas

${GDT_TS_CORS}: ${ALL_COR_STATS}
	${BIN_DIR}/getcors ${CORRELATION} "GDT_TS" $^ > $@

${LDDT_CORS}: ${ALL_COR_STATS}
	${BIN_DIR}/getcors ${CORRELATION} "LDDT" $^ > $@

${CAD_AA_CORS}: ${ALL_COR_STATS}
	${BIN_DIR}/getcors ${CORRELATION} "CAD_AA" $^ > $@

${COR_PLOTS}: ${GDT_TS_CORS} ${LDDT_CORS} ${CAD_AA_CORS}
	${BIN_DIR}/plot-cor.r ${GDT_TS_CORS} "GDT_TS" $(word 1, ${ABC})
	${BIN_DIR}/plot-cor.r ${LDDT_CORS} "LDDT" $(word 2, ${ABC})
	${BIN_DIR}/plot-cor.r ${CAD_AA_CORS} "CAD_AA" $(word 3, ${ABC})
	gs -q -sPAPERSIZE=a4 -dNOPAUSE -dBATCH -sDEVICE=pdfwrite \
-sOutputFile=- ${ABC} > $@
	rm -f ${ABC}

#-------------------------------------------------------------------------------
#Įverčių apibendrinimas

${JOINT_2Djury} ${BENDRAS_2Djury}: ${MERGED_SCORES}
	cat ${MERGED_SCORES} | awk '{print $$5}' > ${JOINT_2Djury}
	${BIN_DIR}/plot-overall.r ${JOINT_2Djury} "X2Djury" ${BENDRAS_2Djury}

${JOINT_CAD_AA} ${BENDRAS_CAD_AA}: ${MERGED_SCORES}
	cat ${MERGED_SCORES} | awk '{print $$4}' > ${JOINT_CAD_AA}
	${BIN_DIR}/plot-overall.r ${JOINT_CAD_AA} "CAD_AA" ${BENDRAS_CAD_AA}

${JOINT_LDDT} ${BENDRAS_LDDT}: ${MERGED_SCORES}
	cat ${MERGED_SCORES} | awk '{print $$3}' > ${JOINT_LDDT}
	${BIN_DIR}/plot-overall.r ${JOINT_LDDT} "LDDT" ${BENDRAS_LDDT}

${JOINT_GDT_TS} ${BENDRAS_GDT_TS}: ${MERGED_SCORES}
	cat ${MERGED_SCORES} | awk '{print $$2}' > ${JOINT_GDT_TS}
	${BIN_DIR}/plot-overall.r ${JOINT_GDT_TS} "GDT_TS" ${BENDRAS_GDT_TS}

${COMPARISON_DIR}/%/merged_scores.txt: ${SUMMARY_DIR}/%.txt ${SCORES_DIR}/%.txt
	test -d ${COMPARISON_DIR}/$* || mkdir ${COMPARISON_DIR}/$*
	${BIN_DIR}/analyze_data.r $< $(word 2, $^) $@

${SCORE_ANALYSIS_DIR}/%.pdf: ${COMPARISON_DIR}/%/merged_scores.txt
	${BIN_DIR}/plot-scores.r $< $@

#------------------------------------------------------------------------------

clean:
	rm -f ${CONTACT_MAPS}
	rm -f ${MERGED_MAPS}
	rm -f ${MERGED_MAP_PLOTS}
	rm -f ${TARGET_SCORES}
	rm -f ${GDT_TS_CORS} ${LDDT_CORS} ${CAD_AA_CORS} ${COR_PLOTS}
	rm -f ${MERGED_SCORES} ${SCORE_HIST} ${MY_SCORE} ${CAD_AA} ${LDDT} ${GDT_TS}
	rm -rf ${COMPARISON_DIR}/*

distclean: clean

